<?php

namespace App\Provider;

use App\Entity\Prize;
use App\Entity\User;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Doctrine\ORM\EntityManager;

class UserProvider implements UserProviderInterface
{
    private $users;
    private $entityManager;

    public function __construct(EntityManager $em)
    {
        $this->entityManager = $em;
    }

    /**
     * Adds a new User to the provider.
     *
     * @param UserInterface $user A UserInterface instance
     *
     * @throws \LogicException
     */
    public function createUser(UserInterface $user)
    {
        if (isset($this->users[strtolower($user->getUsername())])) {
            throw new \LogicException('Another user with the same username already exists.');
        }
        $this->users[strtolower($user->getUsername())] = $user;
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByUsername($username)
    {
        return $this->entityManager->getRepository(User::class)->findOneBy(['username'=>$username]);
    }

    /**
     * {@inheritdoc}
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User)
        {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        $storedUser = $user;

        return new User(
            $storedUser->getId(),
            $storedUser->getUsername(),
            $storedUser->getPassword()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function supportsClass($class)
    {
        return $class === User::class;
    }

    /**
     * Returns the user by given username.
     *
     * @param string $username The username
     *
     * @return User
     *
     * @throws UsernameNotFoundException If user whose given username does not exist.
     */
    private function getUser($username)
    {
        if (!isset($this->users[strtolower($username)])) {
            $ex = new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
            $ex->setUsername($username);

            throw $ex;
        }

        return $this->users[strtolower($username)];
    }

    public function isUserExist($username)
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['username'=>strtolower($username)]);

        if ($user != null && $user instanceof User)
        {
            return $user;
        }
        else
        {
            return false;
        }
    }

    public function validateUsername($username)
    {
        return true;
    }


    public function saveUser($username, $password)
    {
        $user = new User(null, $username, $password);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    public function updateUser(User $user)
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush($user);

        return $user;
    }

    public function getUserByUsername($username)
    {
        return $this->entityManager->getRepository(User::class)->findOneBy(['username'=>$username]);
    }

    public function getUserById($userId)
    {
        return $this->entityManager->getRepository(User::class)->find($userId);
    }

    public function getClassName() {
        return User::class;
    }

    public function getUserInfo($userId)
    {
        $prizes = $this->entityManager->getRepository(Prize::class)->findOneBy(['user' => $userId]);

        $arResult = [
            'bonus' => 0,
            'money' => 0
        ];

        if (!is_array($prizes))
        {
            return $arResult;
        }

        /**
         * @var Prize $prize
         */
        foreach ($prizes as $prize)
        {
            if (in_array($prize->getType(), ['money', 'bonus']))
            {
                $arResult[$prize->getType()] += $prize->getValue();
            }
        }

        return $arResult;
    }
}

