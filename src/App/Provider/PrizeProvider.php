<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 06.11.2018
 * Time: 22:42
 */

namespace App\Provider;

use App\Entity\BonusPrize;
use App\Entity\MoneyPrize;
use App\Entity\Prize;
use App\Entity\ToyPrize;
use App\Entity\User;
use Doctrine\ORM\EntityManager;

class PrizeProvider
{
    private $entityMananger;

    public function __construct(EntityManager $em = null)
    {
        $this->entityMananger = $em;
    }

    public function findAllPrizesByUserId($userId)
    {
        $prizes = $this->entityMananger->getRepository(Prize::class)->findBy([
            'user' => $userId
        ]);

        return $prizes;
    }

    public function findById($prizeId)
    {
        return $this->entityMananger->getRepository(Prize::class)->find($prizeId);
    }

    public function remove($prizeId)
    {
        $prize = $this->findById($prizeId);

        if ($prize instanceof Prize)
        {
            $this->entityMananger->remove($prize);

            $this->entityMananger->flush();
        }
    }

    public function save($arPrize, User $user)
    {
        /**
         * @var Prize $prize
         */
        $prize = new Prize();

        $prize->setId((isset($arPrize['id'])) ? $arPrize['id'] : null);
        $prize->setType($arPrize['type']);
        $prize->setValue($arPrize['value']);
        $prize->setUser($user);

        $this->entityMananger->persist($prize);
        $this->entityMananger->flush();
    }

    public function create4user(User $user)
    {
        $prizeClasses = [BonusPrize::class, MoneyPrize::class, ToyPrize::class];

        $randPrizeClass = $prizeClasses[rand(0, sizeof($prizeClasses) - 1)];

        /**
         * @var Prize $prize
         */
        $prize = new $randPrizeClass;

        $prize->setUser($user);

        $prize->create();

        return $prize;
    }

    public function getPrizeByArray($arPrize)
    {
        if (!isset($arPrize['type']) || !in_array($arPrize['type'], ['money', 'bonus', 'toy']))
        {
            throw new \Exception("Incorrect Prize Type");
        }

        if (!isset($arPrize['value']))
        {
            throw new \Exception("Incorrect Prize Value");
        }

        $entityClass = $this->getClassByType($arPrize['type']);

        /**
         * @var Prize $prize
         */
        $prize = new $entityClass();

        $prize->setValue($arPrize['value']);

        return $prize;
    }

    public function getArrayByPrize(Prize $prize)
    {
        return [
            "id" => $prize->getId(),
            "type" => $prize->getType(),
            "value" => $prize->getValue()
        ];
    }

    public function convert2money($arPrize)
    {
        $userId = $arPrize['user'];

        /**
         * @var BonusPrize $prize
         */
        $prize = $this->getPrizeByArray($arPrize);

        $prize = $prize->convertToMoney();

        $arPrize = $prize->toArray();

        $arPrize['user'] = $userId;

        return $arPrize;
    }

    private function getClassByType($type)
    {
        switch ($type)
        {
            case 'toy':
                $class = ToyPrize::class;
                break;
            case 'money':
                $class = MoneyPrize::class;
                break;
            case 'bonus':
                $class = BonusPrize::class;
                break;
            default:
                throw new \Exception("Incorrect Prize Type");
                break;
        }

        return $class;
    }
}