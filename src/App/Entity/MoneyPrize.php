<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 06.11.2018
 * Time: 22:18
 */

namespace App\Entity;


class MoneyPrize extends Prize
{
    const MIN_PRICE = 1.00;

    const MAX_PRICE = 20.00;

    public $type = 'money';

    public function send()
    {
        // Make Transaction By Bank API
        sleep(2);
    }

    public function create()
    {
        $this->value = $this->calc();

        return $this;
    }

    public function calc()
    {
        return self::getRandPrice();
    }

    public static function getRandPrice()
    {
        return rand(100 * self::MIN_PRICE, 100 * self::MAX_PRICE) / 100;
    }

    public function setPrice($price)
    {
        $this->value = $price;
    }
}