<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 06.11.2018
 * Time: 22:08
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Impl\Prize as PrizeImpl;

/**
 * @ORM\Entity
 * @ORM\Table(name="app_prizes")
 */
class Prize implements PrizeImpl
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=2000, nullable=false)
     */
    public $value;

    /**
     * @ORM\Column(type="string", length=32, unique=true, nullable=false)
     */
    public $type;

    /**
     * @ORM\Column(type="integer")
     */
    private $user;

    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user->getId();
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'type' => $this->getType(),
            'value' => $this->getValue(),
            'user' => $this->getUser()
        ];
    }

    public function create() {}

    public function send() {}

    public function calc() {}

    public function save() {}

}