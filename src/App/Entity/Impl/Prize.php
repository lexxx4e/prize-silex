<?php

/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 06.11.2018
 * Time: 22:10
 */

namespace App\Entity\Impl;

interface Prize {

    public function send();

    public function create();

    public function save();

}