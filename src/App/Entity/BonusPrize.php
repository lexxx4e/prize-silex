<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 06.11.2018
 * Time: 22:20
 */

namespace App\Entity;


class BonusPrize extends Prize
{
    public $type = 'bonus';

    const MAX_BONUS = 10;

    const MIN_BONUS = 1;

    const COEF_CHANGING = 12;

    public function send()
    {
        // Convert Bonus To Money
        // Make Transaction By Bank API
        sleep(2);
    }

    public function create()
    {
        $this->value = $this->calc();

        return $this;
    }

    public function calc()
    {
        return rand(self::MIN_BONUS, self::MAX_BONUS);
    }


    public function convertToMoney()
    {
        $prize = new MoneyPrize();

        $prize->setPrice($this->value * self::COEF_CHANGING);

        return $prize;
    }
}