<?php

namespace App\ControllerProvider;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;

class UserControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $factory = $app['controllers_factory'];

        $factory->post("/info", 'App\Controller\UserController::info');

        return $factory;
    }
}

