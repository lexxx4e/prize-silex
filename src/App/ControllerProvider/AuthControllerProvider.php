<?php

namespace App\ControllerProvider;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;

class AuthControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $factory = $app['controllers_factory'];

        $factory->post("/login", 'App\Controller\AuthController::login');

        return $factory;
    }
}

