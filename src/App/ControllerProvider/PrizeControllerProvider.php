<?php

namespace App\ControllerProvider;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;

class PrizeControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $factory = $app['controllers_factory'];

        $factory->post("/create", 'App\Controller\PrizeController::create');
        $factory->post("/save", 'App\Controller\PrizeController::save');
        $factory->post("/convert", 'App\Controller\PrizeController::convert');
        $factory->post("/send", 'App\Controller\PrizeController::send');
        $factory->post("/all", 'App\Controller\PrizeController::all');

        return $factory;
    }
}

