<?php

use Silex\Application;
use Silex\Provider\SecurityServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app->before(

    function (Request $request) use ($app)
    {
        $app['request'] = $request;

		// check access token validity
//        $headerName = $app['security.jwt']['options']['header_name'];
//
//        $requestToken = $app['request']->headers->get($headerName); // x-access-token
//
//        $id = null;
//        if (!empty($requestToken))
//        {
//            $tokenPrefix = $app['security.jwt']['options']['token_prefix'];
//            $accessToken = trim(str_replace($tokenPrefix, "", $requestToken));
//
//            try
//            {
//                $data = $app['security.jwt.encoder']->decode($accessToken);
//            }
//            catch (\UnexpectedValueException $e)
//            {
//                return $app->json([
//                    'success' => false,
//                    'error' => $e->getMessage(),
//                    'code' => $e->getCode()
//                ], $e->getCode());
//            }
//        }
    }

);

$app->mount('/prize', new \App\ControllerProvider\PrizeControllerProvider());
$app->mount('/auth', new \App\ControllerProvider\AuthControllerProvider());
$app->mount('/user', new \App\ControllerProvider\UserControllerProvider());

$app->after(
    function (Request $request, Response $response)
    {
        $response->headers->set(
            'Content-Type',
            $response->headers->get('Content-Type') . '; charset=UTF-8'
        );

        if (method_exists($response, 'setEncodingOptions'))
        {
            $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);
        }

        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        $response->headers->set('Access-Control-Allow-Headers', 'X-Access-Token, Content-Type');
        return $response;
    }
);