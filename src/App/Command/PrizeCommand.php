<?php

namespace App\Command;

use App\Entity\Prize;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Console\Command\Command;

class PrizeCommand extends Command
{
    protected function configure()
    {
        $this->setName('app:send-prizes')->setDescription('Send Prizes');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $app = require __DIR__.'/../../../app/app.php';

        require __DIR__.'/../../../app/config/config.php';

        /**
         * @var EntityManager $em
         */
        $em = $app['orm.em'];

        $numberPrizes = 10;


        $prizes = $em->getRepository(Prize::class)->findBy(
            [
                'type' => 'money'
            ],
            [
                'id' => 'asc'
            ],
            $numberPrizes
        );

        /**
         * @var Prize $prize
         */
        foreach ($prizes as $prize)
        {
            try
            {
                $em->getConnection()->beginTransaction();

                $prize->send();

                $em->remove($prize);

                $em->flush();

                $em->getConnection()->commit();
            }
            catch (\Exception $e)
            {
                $em->getConnection()->rollBack();
            }

        }

    }

}