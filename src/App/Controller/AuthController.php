<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 06.11.2018
 * Time: 22:32
 */

namespace App\Controller;


use App\Entity\User;
use App\Provider\UserProvider;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;

class AuthController
{
    public function login(Application $app)
    {
        $vars = json_decode($app['request']->getContent(), true);

        try
        {
            $app['monolog']->info($app['request']->getContent());

            $username = (isset($vars['username'])) ? $vars['username'] : null;

            $password = (isset($vars['password'])) ? $vars['password'] : null;

            if (empty($username))
            {
                throw new \Exception("Empty Username");
            }

            if (empty($password))
            {
                throw new \Exception("Empty Password");
            }

            /**
             * @var UserProvider $userProvider
             */
            $userProvider = $app['users'];

            $user = $userProvider->getUserByUsername($username);

            if (!$user instanceof User)
            {
                throw new \Exception("User No Exists");
            }

            if (!$app['security.encoder.digest']->isPasswordValid($user->getPassword(), $password, $user->getSalt()))
            {
                throw new \Exception("Wrong Password");
            }
            else
            {
                return  $app->json([
                    'success' => true,
                    'token' => $app['security.jwt.encoder']->encode([
                            'username' => $user->getUsername() . '',
                            'id' => $user->getId()
                        ]
                    ),
                ], Response::HTTP_OK);
            }
        }
        catch (\Exception $e)
        {
            return $app->json([
                'success' => false,
                'code' => $e->getCode(),
                'error' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }

    }

}