<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 06.11.2018
 * Time: 22:21
 */

namespace App\Controller;

use App\Entity\BonusPrize;
use App\Entity\Prize;
use App\Entity\User;
use App\Provider\PrizeProvider;
use Silex\Application;

class PrizeController
{
    public function create(Application $app)
    {
        try
        {
            /**
             * @var User $user
             */
            $user = $app['user'];

            $prize = (new PrizeProvider($app['orm.em']))->create4user($user);

            return $app->json([
                'success' => true,
                'prize' => $prize->toArray()
            ]);
        }
        catch (\Exception $e)
        {
            return $app->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

    }

    public function convert(Application $app)
    {
        try
        {
            $vars = json_decode($app['request']->getContent(), true);

            $arPrize = $vars['prize'];

            if ($arPrize['type'] == 'bonus')
            {
                $arPrize = (new PrizeProvider($app['orm.em']))->convert2money($arPrize);
            }

            return $app->json([
                'success' => true,
                'message' => $arPrize
            ]);
        }
        catch (\Exception $e)
        {
            return $app->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function save(Application $app)
    {
        try
        {
            $vars = json_decode($app['request']->getContent(), true);

            /**
             * @var User $user
             */
            $user = $app['user'];

            $arPrize = $vars['prize'];

            $prize['username'] = $user->getUsername();

            (new PrizeProvider($app['orm.em']))->save($arPrize, $user);

            return $app->json([
                'success' => true
            ]);
        }
        catch (\Exception $e)
        {
            return $app->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function all(Application $app)
    {
        try
        {
            /**
             * @var User $user
             */
            $user = $app['user'];

            $prizes = (new PrizeProvider($app['orm.em']))->findAllPrizesByUserId($user->getId());

            return $app->json([
                'success' => true,
                'prizes' => $prizes
            ]);
        }
        catch (\Exception $e)
        {
            return $app->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

    }

    public function send(Application $app)
    {
        try
        {
            $vars = json_decode($app['request']->getContent(), true);

            $prizeId = $vars['prize_id'];

            /**
             * @var Prize $prize
             */
            $prize = (new PrizeProvider($app['orm.em']))->findById($prizeId);

            /**
             * @var User $user
             */
            $user = $app['user'];

            $prize->setUser($user);

            $prize->send();

            (new PrizeProvider($app['orm.em']))->remove($prize->getId());

            return $app->json([
                'success' => true
            ]);
        }
        catch (\Exception $e)
        {
            return $app->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

    }

}