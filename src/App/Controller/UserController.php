<?php

namespace App\Controller;

use App\Entity\User;
use App\Provider\UserProvider;
use Silex\Application;

class UserController
{
    public function info(Application $app)
    {
        try
        {
            /**
             * @var User $user
             */
            $user = $app['user'];

            $info = (new UserProvider($app['orm.em']))->getUserInfo($user->getId());

            return $app->json([
                'success' => true,
                'info' => $info
            ]);
        }
        catch (\Exception $e)
        {
            return $app->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

    }


}