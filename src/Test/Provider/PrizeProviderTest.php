<?php

/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 06.11.2018
 * Time: 23:08
 */

namespace App\Test\Provider;

use App\Entity\BonusPrize;
use App\Entity\MoneyPrize;
use App\Provider\PrizeProvider;
use PHPUnit\Framework\TestCase;

class PrizeProviderTest extends TestCase
{
    public function testPrizeConvert()
    {
        $bonus = MoneyPrize::getRandPrice();

        $arBonusPrize = [
            'id' => null,
            'type' => 'bonus',
            'value' => $bonus,
            'user' => 0
        ];

        $arMoneyPrize = (new PrizeProvider())->convert2money($arBonusPrize);

        $money = $arMoneyPrize['value'];

        $this->assertTrue($money / BonusPrize::COEF_CHANGING === $bonus, 'Crush Converting Bonus to Money');
    }
}