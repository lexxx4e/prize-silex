<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Symfony\Component\Console\Application;

$app = require_once __DIR__.'/app/app.php';

$entityManager = $app['orm.em'];
$helperSet = ConsoleRunner::createHelperSet($entityManager);

$cli = new Application('Doctrine Command Line Interface', \Doctrine\ORM\Version::VERSION);
$cli->setCatchExceptions(true);
$cli->setHelperSet($helperSet);

ConsoleRunner::addCommands($cli);

$cli->run();