<?php

use Silex\Provider\DoctrineServiceProvider;
use Dflydev\Provider\DoctrineOrm\DoctrineOrmServiceProvider;

require __DIR__.'/config/config.php';

$app['security.jwt'] = $jwt_config;

$app->register(new Silex\Provider\MonologServiceProvider(), $monolog_config);

$app->register(new DoctrineServiceProvider(), $db_config);
$app->register(new DoctrineOrmServiceProvider(), $db_orm);

$app['users'] = function () use ($app) {
    return new App\Provider\UserProvider($app['orm.em']);
};

$app['security.firewalls'] = [
    'login' => [
        'pattern' => 'auth/login',
        'anonymous' => true,
    ],
    'secured' => [
        'pattern' => '^.*$',
        'logout' => ['logout_path' => '/logout'],
        'users' => $app['users'],
        'jwt' => [
            'use_forward' => true,
            'require_previous_session' => false,
            'stateless' => true,
        ]
    ]
];


//$app['security.role_hierarchy'] = [
//    'ROLE_SUPER_ADMIN' => ['ROLE_ADMIN', 'ROLE_USER'],
//    'ROLE_ADMIN' => ['ROLE_USER']
//];


$app->register(new Silex\Provider\SecurityServiceProvider(), [
    'security.firewalls'        => $app['security.firewalls']
]);



$app->register(new Silex\Provider\SecurityJWTServiceProvider());