<?php

$jwt_config = [
    'secret_key' => '',
    'life_time'  => 60 * 60 * 24 * 7,
    'algorithm'  => ['HS256'],
    'options'    => [
        'header_name'  => 'X-Access-Token',
        'username_claim' => 'username', // default name, option specifying claim containing username
        'token_prefix' => 'Bearer',
    ]
];

$db_config = [
    'db.options'    => [
        'driver'    => 'pdo_mysql',
        'host'      => 'localhost',
        'dbname'    => 'prize',
        'user'      => 'root',
        'password'  => '',
        'charset'       => 'utf8',
        'driverOptions' => [
            1002 => 'SET NAMES utf8',
        ],
    ]
];

$db_orm = [
    'orm.proxies_dir'             => __DIR__ . '/../../src/App/Entity/Proxy',
    'orm.auto_generate_proxies'   => true, // false for productive usage
    'orm.em.options'              => [
        'mappings' => [
            [
                'type'                         => 'annotation',
                'namespace'                    => 'App\\Entity\\',
                'path'                         => __DIR__.'/../../src/App/Entity',
                'use_simple_annotation_reader' => false,
            ],
        ],
    ]
];

$monolog_config = [
    'monolog.logfile' => __DIR__.'/../../log/develop.log',
];

$app['debug'] = true;